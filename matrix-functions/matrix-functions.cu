#pragma once
#include "matrix-functions.h"
#include <iostream>

__global__ void multiplicacaoMatriz(float* matrizEntrada, float* vetorEntrada,
	float* matrizResultado, int linhasMatriz, int colunasMatriz,
	int linhasVetor, int colunasVetor, int linhasResultado,
	int colunasResultado) {


	// for para omar os elementos
	// sem usar threads
	float soma = 0;
	int startMatriz = (blockIdx.x * colunasMatriz);
	int endMatriz = startMatriz + colunasMatriz;

	int indexMaxMatriz = (linhasMatriz * colunasMatriz);
	int indexMaxVetor = (linhasVetor * colunasVetor);

	if (startMatriz < indexMaxMatriz && endMatriz <= indexMaxMatriz) {
		for (int incrementador = startMatriz; incrementador < endMatriz; incrementador++) {
			soma += (matrizEntrada[incrementador] * vetorEntrada[incrementador - startMatriz]);
		}
	}


	int posResultado = blockIdx.x * colunasResultado + blockIdx.y;
	int indexMaxResultado = (linhasResultado * colunasResultado);
	if (posResultado < indexMaxResultado) {
		matrizResultado[posResultado] += soma;
	}

}

__global__ void tcc_matrixMultiStream(unsigned int numeroColunas, float* matriz1, float* matriz2, float* matrizResultado) {
	float soma = 0.0f;
	for (unsigned int c = 0; c < numeroColunas; c++) {
		// soma os dados da matrix
		soma += matriz1[c] * matriz2[c];
	}
	// arrumar
	matrizResultado[0] = soma;

}


// kernel X = colunas matrix
__global__ void  tcc_MatrixVectorMultiplyThread(float* matrizEntrada, float* vetorEntrada,
	float* matrizResultado, int linhasMatriz, int colunasMatriz) {


	// determianr a posicao dos ponteiros
	unsigned int posicaoMatriz = threadIdx.x;
	unsigned int posicaoVetor = threadIdx.x;

	// calcular a multiplicacao

	// definir a soma como uma meoria compartilhada
	__shared__ float soma;
	soma = 0.0f;
	__syncthreads();

	//soma += matrizEntrada[posicaoMatriz] * vetorEntrada[posicaoVetor];

	soma = soma + 1;
	__syncthreads();

	// sincronar thread
	matrizResultado[0] = soma;
	
}


void multiplicacaoMatrizStreamThread(Matriz* matriz, Matriz* vetor, Matriz* matrizResultadoGPU, float* ptrMatrix,
	float* ptrMatrix2, float* ptrMatrixResultado) {
	quebraLinha();
	std::cout << "Teste matriz Stream com uso de thread";
	quebraLinha();
	// cria as streams
	int size = SIZE_MATRIX;
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}

	dim3 numeroBlocos2D(matriz->numeroLinhas,
		matriz->numeroColunas);

	cudaMemcpy(ptrMatrix2, vetor->matriz,
		vetor->numeroLinhas * sizeof(float), cudaMemcpyHostToDevice);

	cudaDeviceSynchronize();

	// para cada stream
	for (int a = 0; a < size; a++) {
		// executar copia de memoria Async
		// copia so uma linha da matrix

		// deslocamento das matrizes
		long deslmatriz = (a * matriz->numeroColunas);

		long deslMatrizResultado = a;

		cudaMemcpyAsync(ptrMatrix + deslmatriz, matriz->matriz + deslmatriz,
			matriz->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);


		tcc_MatrixVectorMultiplyThread << <1, numeroBlocos2D, 1024, stream[a] >> > (ptrMatrix + deslmatriz,
			ptrMatrix2, ptrMatrixResultado + deslMatrizResultado, matriz->numeroLinhas, matriz->numeroColunas);


		// precisa copiar apenas um valor do vetor
		cudaMemcpyAsync(matrizResultadoGPU->matriz + deslMatrizResultado, ptrMatrixResultado + deslMatrizResultado,
			sizeof(float), cudaMemcpyDeviceToHost, stream[a]);
		// executar o Kernel
		// cuda de memoria de volta para o host Async

	}
	// sincronizar os kernels depois das chamadas
	// contar o tempo de execucao
	cudaDeviceSynchronize();

	for (int a = 0; a < size; a++) {
		int streamStatus = cudaStreamQuery(stream[a]);
		if (streamStatus != 0) {
			std::cout << "stream " << a << " n�o terminada.";
		}
	}


}

void multiplicacaoMatrizStream(Matriz* matriz, Matriz* vetor, Matriz* matrizResultadoGPU, float* ptrMatrix,
	float* ptrMatrix2, float* ptrMatrixResultado) {
	quebraLinha();
	std::cout << "Teste matriz Stream sem uso de thread";
	quebraLinha();
	// cria as streams
	int size = SIZE_MATRIX;
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}

	dim3 numeroBlocos2D(matriz->numeroLinhas,
		matriz->numeroColunas);

	cudaMemcpy(ptrMatrix2, vetor->matriz,
		vetor->numeroLinhas * sizeof(float), cudaMemcpyHostToDevice);

	cudaDeviceSynchronize();

	// para cada stream
	for (int a = 0; a < size; a++) {
		// executar copia de memoria Async
		// copia so uma linha da matrix

		// deslocamento das matrizes
		long deslmatriz = (a * matriz->numeroColunas);

		long deslMatrizResultado = a;

		cudaMemcpyAsync(ptrMatrix + deslmatriz, matriz->matriz + deslmatriz,
			matriz->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);


		tcc_matrixMultiStream << <1, numeroBlocos2D, 1024, stream[a] >> > (matriz->numeroColunas, ptrMatrix + deslmatriz,
			ptrMatrix2, ptrMatrixResultado + deslMatrizResultado);


		// precisa copiar apenas um valor do vetor
		cudaMemcpyAsync(matrizResultadoGPU->matriz + deslMatrizResultado, ptrMatrixResultado + deslMatrizResultado,
			sizeof(float), cudaMemcpyDeviceToHost, stream[a]);
		// executar o Kernel
		// cuda de memoria de volta para o host Async

	}
	// sincronizar os kernels depois das chamadas
	// contar o tempo de execucao
	cudaDeviceSynchronize();

	for (int a = 0; a < size; a++) {
		int streamStatus = cudaStreamQuery(stream[a]);
		if (streamStatus != 0) {
			std::cout << "stream " << a << " n�o terminada.";
		}
	}


}

