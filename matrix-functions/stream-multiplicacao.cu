#include <iostream>

#include <stdio.h>

#include "stream-multiplicacao.h"
#include "../matrix-utils/matrix-utils.h"
#include "../time-utils/time-util.h"
#include "../matrix-functions/matrix-functions.h"





void testeGenerico(int typeTeste, bool printMatrix, bool printTime) {

	clearResult();

	// criar as matrizes do host
	switch (typeTeste) {
		case 0:
			std::cout << "Teste de Multi Stream inicial, sem usar threads" << std::endl;
			break;
		case 1: 
			std::cout << "Teste de Multi Stream inicial, com threads" << std::endl;
			break;
	}


	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);

	printf("Device Number: %d\n", 0);
	printf("  Device name: %s\n", prop.name);
	printf("  Memory Clock Rate (KHz): %d\n",
		prop.memoryClockRate);
	printf("  Memory Bus Width (bits): %d\n",
		prop.memoryBusWidth);

	printf(" Device asyncEngineCount : %d\n",
		prop.asyncEngineCount);

	// define o teste
	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = QTD_TESTS;
	test->sizeMatrix = SIZE_MATRIX;

	int size = test->sizeMatrix;

	// criar as struct das matrizes
	Matriz* matriz1 = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matriz1);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* matriz2 = gerarStructMatriz(test->sizeMatrix, 1);
	alocarMatrizCudaStream(matriz2);

	if (printMatrix) {
		std::cout << "matrix 1 : ";
		quebraLinha();
		printMatriz(matriz1);
		quebraLinha();
		std::cout << "matrix 2 : ";
		quebraLinha();
		printMatriz(matriz2);
	}


	if (podeMultiplicar(matriz1, matriz2)) {
		std::cout << "pode multiplicar";
		quebraLinha();
	}
	else {
		std::cout << "Nao pode multiplicar";
		quebraLinha();
		return;
	}

	Matriz* matrizResultado = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultado);

	Matriz* matrizResultadoGPU = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultadoGPU);

	//std::cout << "matrix Resulktado inciail : ";
	//quebraLinha();
	//printMatriz(matrizResultado);


	// Gera e exibe a matriz resultante da multiplicação
	// Matriz* matrizResultanteDevice = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);



	// Declara os ponteiro a serem passados para o device
	float* ptrMatrix;
	float* ptrMatrix2;
	float* ptrMatrixResult;
	int cudaStatus;



	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus = cudaMalloc(&ptrMatrix, matriz1->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaStatus = cudaMalloc(&ptrMatrix2, matriz2->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaStatus = cudaMalloc(&ptrMatrixResult, matrizResultadoGPU->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}


	multiplicaoMatrixVetor(matriz1, matriz2, matrizResultado);

	// para cada teste
	for (int t = 0; t < size; t++) {


		startTime();
		
		switch (typeTeste)
		{
		case 0: 
			multiplicacaoMatrizStreamThread(matriz1, matriz2, matrizResultadoGPU, ptrMatrix, ptrMatrix2, ptrMatrixResult);
			break;
		case 1:
			multiplicacaoMatrizStream(matriz1, matriz2, matrizResultadoGPU, ptrMatrix, ptrMatrix2, ptrMatrixResult);
			break;
		default:
			break;
		}


		stopTime(true);
		if (printMatrix) {
			std::cout << "Vetor Gpu resultado:";
			quebraLinha();
			printMatriz(matrizResultadoGPU);

			std::cout << "VETOR CPU: ";
			quebraLinha();
			std::cout << "Resultado:" << std::endl;
			printMatriz(matrizResultado);
			quebraLinha();
		}

		if (isEqualMatrix(matrizResultadoGPU, matrizResultado)) {
			std::cout << "Teste concluido " << t << " com sucesso";
		}
		else {
			std::cout << "Ocorreu um erro no teste " << t;
		}


		if (printTime) {
	
			quebraLinha();
			printLastTimeResult();
		}
		
	}
	quebraLinha();
	printTimeResult();


	cudaFree(ptrMatrix);
	cudaFree(ptrMatrix2);
	cudaFree(ptrMatrixResult);

	freeMatrizStream(matriz1);
	freeMatrizStream(matriz2);
	freeMatrizStream(matrizResultado);


}


