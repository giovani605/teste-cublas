#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_functions.h>
#include <device_launch_parameters.h>
#include <device_atomic_functions.h>
#include "common/common-utils.h"
#include "matrix-utils/matrix-utils.h"



//__global__ void multiplicacaoMatriz(Matriz* matrizEntrada, Matriz* matrizVetor,
//		Matriz* matrizResultado)

__global__ void multiplicacaoMatriz(float* matrizEntrada, float* vetorEntrada,
	float* matrizResultado, int linhasMatriz, int colunasMatriz,
	int linhasVetor, int colunasVetor, int linhasResultado,
	int colunasResultado);

__global__ void tcc_matrixMultiStream(unsigned int numeroColunas, float* matriz1, float* matriz2, float* matrizResultado);

// cada chamada faz uma aprte da multiplicaocao de matriz vetor
__global__ void  tcc_MatrixVectorMultiplyThread(float* matrizEntrada, float* vetorEntrada,
	float* matrizResultado, int linhasMatriz, int colunasMatriz);


void multiplicacaoMatrizStreamThread(Matriz* matriz, Matriz* vetor, Matriz* matrizResultadoGPU, float* ptrMatrix,
						float* ptrMatrix2, float* ptrMatrixResultado);

void multiplicacaoMatrizStream(Matriz* matriz, Matriz* vetor, Matriz* matrizResultadoGPU, float* ptrMatrix,
	float* ptrMatrix2, float* ptrMatrixResultado);


