

#pragma once
#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include "device_launch_parameters.h"


#include "../matrix-utils/matrix-utils.h"
#include "../common/common-utils.h"
#include "matrix-functions.h"

void testeGenerico(int typeTeste = 0, bool printMatrix = false, bool printTime = true);

