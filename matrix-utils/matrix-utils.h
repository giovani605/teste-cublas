// aqui a gente pode desenvolver as coisas de matrix
#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include "../common/common-utils.h"



void printMatriz(float** matriz, int M, int tamanho);

Matriz* gerarStructMatriz(int linhas, int colunas);

void desalocaStructMatriz(Matriz* matriz);

void preencherMatrizComNumeracaoCreescente(Matriz* matriz);

void printMatriz(Matriz* matriz);

void quebraLinha();

bool verificarMultMatrizVetor(Matriz* m, Matriz* v);

void alocarMatrizCuda(Matriz* matriz, float* ponteiro);

void alocarMatrizCudaStream(Matriz* matriz, bool random = true);

void simularMultiplicacaoNoHost(Matriz* matriz, Matriz* vetor);

void tamanhoMatriz(Matriz* matriz);

bool testaSomarMatrizResultado(Matriz* matriz1, Matriz* matriz2, Matriz* matrizResultado);

void freeMatrizStream(Matriz* matriz);

void multiplicaoMatrixVetor(Matriz* matriz1, Matriz* matriz2, Matriz* matrizResultado);

bool podeMultiplicar(Matriz* m1, Matriz* m2);

bool isEqualMatrix(Matriz* matriz1, Matriz* matriz2);


int calculateIndex(int x, int y, int colunas);

/**
 *	@param {float**} ponteiroDeRetorno Como funções globais só podem ser void, aqui será passado um ponteiro de ponteiro. Ou seja, a função pai irá criar um ponteiro que irá
 *	armazenar o local de memória da posição zero do vetor, porém para acessar e alterar essa variável é necessário o endereço de memória da variável ponteiro da função pai,
 *	sendo assim um ponteiro de ponteiro;
 *	@param {unsigned int} linha Linha da matrix que seja deseja acessar;
 *	@param {unsigned int} coluna Coluna da matrix que se deseja acessar;
 *	@param {matrix_t*} matrixAcessada Struct que contem a matrix a ser acessa na localização passada por parâmetro;
 */
__host__ __device__ void acessaVetorComoMatrix(
	float** ponteiroDaPosicaoDesejada, unsigned int linha,
	unsigned int coluna, Matriz* matrixAcessada);
