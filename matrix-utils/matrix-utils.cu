#include <iostream>
#include <cuda_runtime.h>
#include "matrix-utils.h"
#include <cuda.h>
#include <cuda_runtime.h>

void quebraLinha() {
	std::cout << std::endl;
}

Matriz* gerarStructMatriz(int linhas, int colunas) {

	Matriz* structSendoGerada = (Matriz*)malloc(sizeof(Matriz));

	structSendoGerada->numeroColunas = colunas;

	structSendoGerada->numeroLinhas = linhas;

	structSendoGerada->tamanhoFloatArray = colunas * linhas * sizeof(float);

	// TODO TIRAR ESTA ALOCACAO DAQUI
	structSendoGerada->matriz = (float*)malloc(colunas * linhas * sizeof(float));

	return structSendoGerada;
}

void printMatriz(Matriz* matriz) {
	float* posicaoAcessada;

	for (int contadorLinhas = 0; contadorLinhas < matriz->numeroLinhas;
		contadorLinhas++) {
		for (int contadorColunas = 0; contadorColunas < matriz->numeroColunas;
			contadorColunas++) {
			printf("%f ", matriz->matriz[matriz->numeroColunas * contadorLinhas + contadorColunas]);
		}
		quebraLinha();
	}
}

void desalocaStructMatriz(Matriz* matriz) {
	free(matriz->matriz);
	free(matriz);
}

void freeMatrizStream(Matriz* matriz) {
	cudaFreeHost(matriz->matriz);
	free(matriz);
}

void preencherMatrizComNumeracaoCreescente(Matriz* matriz) {
	float* posicaoDesejada;

	for (int contadorLinha = 0; contadorLinha < matriz->numeroLinhas;
		contadorLinha++) {

		for (int contadorColuna = 0; contadorColuna < matriz->numeroColunas;
			contadorColuna++) {

			acessaVetorComoMatrix(&posicaoDesejada, contadorLinha,
				contadorColuna, matriz);

			*posicaoDesejada = (contadorLinha * matriz->numeroColunas)
				+ contadorColuna;
		}
	}
}

bool verificarMultMatrizVetor(Matriz* m, Matriz* v) {
	if (m->numeroColunas == v->numeroLinhas) {
		return true;
	}
	return false;
}

void alocarMatrizCuda(Matriz* matriz, float* ponteiro) {
	//	cudaMalloc(&matriz, m->size);
}

void tamanhoMatriz(Matriz* matriz) {
	std::cout << " tamanho da matriz " << matriz->tamanhoFloatArray / 1000000 << " MBs" << std::endl;
}

void alocarMatrizCudaStream(Matriz* matriz, bool random) {
	cudaMallocHost(&matriz->matriz, (matriz->numeroColunas * matriz->numeroLinhas) * sizeof(float));
	for (unsigned int a = 0; a < matriz->tamanhoFloatArray; a++) {
		if (random) {
			matriz->matriz[a] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 10));
		}
		else {
			matriz->matriz[a] = a;
		}

	}
}

void simularMultiplicacaoNoHost(Matriz* matriz, Matriz* vetor) {
	if (!verificarMultMatrizVetor(matriz, vetor)) {
		std::cout
			<< "Impossível fazer simulação com matrizes do tamanho do passado";
		quebraLinha();
		return;
	}
	else {
		std::cout << "Simulando multiplicação no host!";
		quebraLinha();
	}

	Matriz* resultante = gerarStructMatriz(matriz->numeroLinhas,
		vetor->numeroColunas);

	/* Realizando multiplicação no host **/
	float* posicaoAcessadaMatrizResultante, * posicaoAcessadaVetor,
		* posicaoAcessadaMatriz;
	for (unsigned int varredor = 0; varredor < matriz->numeroColunas;
		varredor++) {
		for (unsigned int contadorLinha = 0;
			contadorLinha < resultante->numeroLinhas; contadorLinha++) {
			for (unsigned int contadorColuna = 0;
				contadorColuna < resultante->numeroColunas;
				contadorColuna++) {
				acessaVetorComoMatrix(&posicaoAcessadaMatrizResultante,
					contadorLinha, contadorColuna, resultante);

				acessaVetorComoMatrix(&posicaoAcessadaMatriz, contadorLinha,
					varredor, matriz);

				acessaVetorComoMatrix(&posicaoAcessadaVetor, varredor,
					contadorColuna, vetor);

				*posicaoAcessadaMatrizResultante =
					*posicaoAcessadaMatrizResultante
					+ *posicaoAcessadaMatriz
					* *posicaoAcessadaVetor;
			}
		}
	}

	// Exibe a matriz resultante do host após o calculo
	std::cout << "Exibe martiz resultante host após o processamento [HOST]";
	quebraLinha();
	printMatriz(resultante);
	desalocaStructMatriz(resultante);

	/* Realizando multiplicação no host **/
}

__host__ __device__ void acessaVetorComoMatrix(
	float** ponteiroDaPosicaoDesejada, unsigned int linha,
	unsigned int coluna, Matriz* matrixAcessada) {

	if (linha > matrixAcessada->numeroLinhas
		|| coluna > matrixAcessada->numeroColunas) {

		*ponteiroDaPosicaoDesejada =
			&(matrixAcessada->matriz[matrixAcessada->numeroColunas
				* matrixAcessada->numeroLinhas - 1]);

	}
	else {

		*ponteiroDaPosicaoDesejada = &(matrixAcessada->matriz[linha
			* matrixAcessada->numeroColunas + coluna]);

	}
}
bool testaSomarMatrizResultado(Matriz* matriz1, Matriz* matriz2, Matriz* matrizResultado) {
	bool flag = true;
	for (int a = 0; a < matrizResultado->numeroColunas * matrizResultado->numeroLinhas; a++) {
		if (matrizResultado->matriz[a] != (matriz1->matriz[a] + matriz2->matriz[a])) {
			flag = false;
			std::cout << "Erro: cuda: " << matrizResultado->matriz[a] << " resCpu " << (matriz1->matriz[a] + matriz2->matriz[a]) << " index  " << a << " " << matriz1->matriz[a] << " " << matriz2->matriz[a];
			quebraLinha();
		}
	}
	return flag;


}

int calculateIndex(int x, int y, int colunas) {
	return (y * colunas) + x;
}

void multiplicaoMatrixVetor(Matriz* matriz1, Matriz* matriz2, Matriz* matrizResultado) {

	for (int a = 0; a < matrizResultado->numeroColunas; a++) {
		for (int b = 0; b < matrizResultado->numeroLinhas; b++) {
			// para cada indice matrix resultado

			float soma = 0.0f;
			for (int c = 0; c < matriz1->numeroColunas; c++) {
				// soma os dados da matrix
				soma += matriz1->matriz[calculateIndex(c, b, matriz1->numeroColunas)] * matriz2->matriz[c];
			}
			// arrumar
			matrizResultado->matriz[calculateIndex(a, b, matrizResultado->numeroColunas)] = soma;
		}
	}

}


bool podeMultiplicar(Matriz* m1, Matriz* m2) {
	return m1->numeroColunas == m2->numeroLinhas;
}


bool isEqualMatrix(Matriz* matriz1, Matriz* matriz2) {
	bool flag = true;
	for (int a = 0; a < matriz1->numeroColunas * matriz1->numeroLinhas; a++) {
		if (matriz1->matriz[a] != matriz2->matriz[a]) {
			flag = false;
			std::cout << "Erro: cuda: " << matriz1->matriz[a] << " resCpu " << matriz2->matriz[a] << " index  " << a;
			quebraLinha();
		}
	}
	return flag;
}