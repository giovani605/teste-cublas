// Utilidade de time para facilitar calcualr o tempo
#include <string.h>
#include <vector> 
#include <time.h>
#include <iostream>
#include <chrono>

void startTime();

long stopTime(bool flagAddResult);

void printTimeResult();

void clearResult();

void printLastTimeResult();

void addTimeResult(long resultado);
