

#include "time-util.h"

using namespace std;

std::vector<long> result;

std::chrono::time_point<std::chrono::high_resolution_clock> __time_start;

void startTime() {
	__time_start = chrono::high_resolution_clock::now();
}

void addTimeResult(long resultado) {
	result.push_back(resultado);
}

long stopTime(bool flagAddResult) {
	auto time_end = chrono::high_resolution_clock::now().time_since_epoch();
	auto time_result1 = chrono::duration_cast<chrono::microseconds>(__time_start.time_since_epoch()).count();
	auto time_result2 = chrono::duration_cast<chrono::microseconds>(time_end).count();
	long result_final = time_result2 - time_result1;

	cout << "tempo create matrix " << result_final << " microseconds" << endl;
	if (flagAddResult) {
		addTimeResult(result_final);
	}
	return result_final;
}


void printTimeResult() {
	for (const int a : result) {
		cout << a << endl;
	}
}

void printLastTimeResult() {

	cout << "Ultimo resultado: " << result.at(result.size()-1) << endl;

}

void clearResult() {
	result.clear();
}



