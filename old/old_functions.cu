﻿/**
	Guardei nesse arquivo as funcoes antigas de matrix que talvez sejam reinseridas no projeto
**/


#include <iostream>

#include <stdio.h>

#include "../matrix-utils/matrix-utils.h"
#include "../time-utils/time-util.h"
#include "../matrix-functions/matrix-functions.h"

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <cuda_device_runtime_api.h>

// Adicionar aqui a declaracoes de funcoes de matrixes que usam stream
// tbm inserir aqui a chamada da funcao encapsulada


__host__ __device__  int toIndex(int x, int y, int colunas);

__global__ void matrixMultiStream(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante);

__global__ void matrixMultiStreamKernel(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante);

__global__ void matrixSomaStream(unsigned int numeroColunas, float* matriz1, float* matriz2,
	float* matrizResultado);


// separar cada conjunto de multiplicacao em uma stream, por enquanto nao usar diversos kernels
__global__ void matrixMultiStream(unsigned int numeroColunas, float* matriz1, float* matriz2, float* matrizResultado);

void testeMultiplicaoStream();

void testeMultiplicaoStreamKernel();



// tem algo de errado nesse codigo
__global__ void matrixMultiStream(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante) {

	unsigned int posicaoResultante = threadIdx.x * numeroColunasVetor
		+ threadIdx.y;
	unsigned int posicaoMatriz = threadIdx.x * numeroColunasMatriz + blockIdx.x;
	unsigned int posicaoVetor = blockIdx.x * numeroColunasVetor + threadIdx.y;

	float valorResultante = resultante[posicaoResultante], valorMatriz =
		matriz[posicaoMatriz], valorVetor = vetor[posicaoVetor];

	resultante[posicaoResultante] = resultante[posicaoResultante]
		+ matriz[posicaoMatriz] * vetor[posicaoVetor];

}


// usa o bloco do tamanho da matriz de entrada
__global__ void matrixMultiStreamKernel(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante) {


	// determianr a posicao dos ponteiros
	unsigned int posicaoResultante = threadIdx.x * numeroColunasVetor
		+ threadIdx.y;
	unsigned int posicaoMatriz = threadIdx.x * numeroColunasMatriz + blockIdx.x;
	unsigned int posicaoVetor = blockIdx.x * numeroColunasVetor + threadIdx.y;

	// calcular a multiplicacao

	// siuncornizar as thread para tentar usar a memoria do block/thread

	// jogar o resultado na matrix resultante
	float valorResultante = resultante[posicaoResultante], valorMatriz =
		matriz[posicaoMatriz], valorVetor = vetor[posicaoVetor];

	resultante[posicaoResultante] = resultante[posicaoResultante] + matriz[posicaoMatriz] * vetor[posicaoVetor];
	// resultante[posicaoResultante] = 1;
}


// suposto kernel 1-1
// recebe os ponteiros do vetor e executa a soma por X unidades
__global__ void matrixSomaStream(unsigned int numeroColunas, float* matriz1, float* matriz2, float* matrizResultado) {
	for (int a = 0; a < numeroColunas; a++) {
		matrizResultado[a] = matriz1[a] + matriz2[a];
	}
}

// recebe os 2 vetores que vao ser multiplicados e salva o resultado no ponteiro da matrix de resultado
__global__ void matrixMultiStream(unsigned int numeroColunas, float* matriz1, float* matriz2, float* matrizResultado) {
	float soma = 0.0f;
	for (unsigned int c = 0; c < numeroColunas; c++) {
		// soma os dados da matrix
		soma += matriz1[c] * matriz2[c];
	}
	// arrumar
	matrizResultado[0] = soma;

}


void testeStream() {
	std::cout << "Teste de Stream" << std::endl;


	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);

	printf("Device Number: %d\n", 0);
	printf("  Device name: %s\n", prop.name);
	printf("  Memory Clock Rate (KHz): %d\n",
		prop.memoryClockRate);
	printf("  Memory Bus Width (bits): %d\n",
		prop.memoryBusWidth);

	printf(" Device asyncEngineCount : %d\n",
		prop.asyncEngineCount);

	// define o teste
	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = 1;
	test->sizeMatrix = SIZE_MATRIX;

	int size = test->sizeMatrix;

	// cria as streams
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}

	// criar as struct das matrizes
	Matriz* matriz1 = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matriz1);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* matriz2 = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matriz2);
	std::cout << "matrix 1 : ";
	quebraLinha();
	//printMatriz(matriz1);
	quebraLinha();
	std::cout << "matrix 2 : ";
	quebraLinha();
	//printMatriz(matriz2);


	Matriz* matrizResultado = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matrizResultado);


	// Gera e exibe a matriz resultante da multiplicação
	// Matriz* matrizResultanteDevice = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);



	// Declara os ponteiro a serem passados para o device
	float* ptrMatrix;
	float* ptrMatrix2;
	float* ptrMatrixResult;
	int cudaStatus;



	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus = cudaMalloc(&ptrMatrix, matriz1->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaStatus = cudaMalloc(&ptrMatrix2, matriz2->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaStatus = cudaMalloc(&ptrMatrixResult, matrizResultado->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}



	// para cada teste
	for (int t = 0; t < test->qtdTestes; t++) {
		// iniciar contabilizacao de tempo
		startTime();
		// para cada stream
		for (int a = 0; a < size; a++) {
			// executar copia de memoria Async
			// copia so uma linha da matrix

			// deslocamento das matrizes
			long deslMatriz1 = (a * matriz1->numeroColunas);

			long deslMatriz2 = (a * matriz2->numeroColunas);

			long deslMatrizResultado = (a * matrizResultado->numeroColunas);

			cudaMemcpyAsync(ptrMatrix + deslMatriz1, matriz1->matriz + deslMatriz1,
				matriz1->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);

			cudaMemcpyAsync(ptrMatrix2 + deslMatriz2, matriz2->matriz + deslMatriz2,
				matriz2->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);

			/*
			matrixSomaStream << <1, 1, 0, stream[a] >> > (matriz1->numeroColunas, ptrMatrix + deslMatriz1,
				ptrMatrix2 + deslMatriz2, ptrMatrixResult + deslMatrizResultado);

				*/

			cudaMemcpyAsync(matrizResultado->matriz + deslMatrizResultado, ptrMatrixResult + deslMatrizResultado,
				matrizResultado->numeroColunas * sizeof(float), cudaMemcpyDeviceToHost, stream[a]);
			// executar o Kernel
			// cuda de memoria de volta para o host Async

		}
		// sincronizar os kernels depois das chamadas
		// contar o tempo de execucao
		cudaDeviceSynchronize();
		stopTime(true);
		for (int a = 0; a < size; a++) {
			int streamStatus = cudaStreamQuery(stream[a]);
			if (streamStatus != 0) {
				std::cout << "stream " << a << " não terminada.";
			}
		}


		quebraLinha();


		std::cout << "Resultado:" << std::endl;
		//printMatriz(matrizResultado);
		quebraLinha();

		if (testaSomarMatrizResultado(matriz1, matriz2, matrizResultado)) {
			std::cout << "Teste " << t << " realizado com sucesso ";
		}
		std::cout << "REsultado do tempo ";
		quebraLinha();
		printTimeResult();
	}


	for (int i = 0; i < size; ++i) {
		cudaStreamDestroy(stream[i]);
	}

	cudaFree(ptrMatrix);
	cudaFree(ptrMatrix2);
	cudaFree(ptrMatrixResult);

	freeMatrizStream(matriz1);
	freeMatrizStream(matriz2);
	freeMatrizStream(matrizResultado);

}

void testeMultiplicaoStream() {
	clearResult();

	// criar as matrizes do host
	std::cout << "Teste de Multi Stream inicial, sem usar threads" << std::endl;


	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);

	printf("Device Number: %d\n", 0);
	printf("  Device name: %s\n", prop.name);
	printf("  Memory Clock Rate (KHz): %d\n",
		prop.memoryClockRate);
	printf("  Memory Bus Width (bits): %d\n",
		prop.memoryBusWidth);

	printf(" Device asyncEngineCount : %d\n",
		prop.asyncEngineCount);

	// define o teste
	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = QTD_TESTS;
	test->sizeMatrix = SIZE_MATRIX;

	int size = test->sizeMatrix;

	// cria as streams
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}


	// criar as struct das matrizes
	Matriz* matriz1 = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matriz1);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* matriz2 = gerarStructMatriz(test->sizeMatrix, 1);
	alocarMatrizCudaStream(matriz2);

	/*
	std::cout << "matrix 1 : ";
	quebraLinha();
	printMatriz(matriz1);
	quebraLinha();
	std::cout << "matrix 2 : ";
	quebraLinha();
	printMatriz(matriz2);
	*/


	if (podeMultiplicar(matriz1, matriz2)) {
		std::cout << "pode multiplicar";
		quebraLinha();
	}
	else {
		std::cout << "Nao pode multiplicar";
		quebraLinha();
		return;
	}

	Matriz* matrizResultado = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultado);

	Matriz* matrizResultadoGPU = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultadoGPU);

	//std::cout << "matrix Resulktado inciail : ";
	//quebraLinha();
	//printMatriz(matrizResultado);


	// Gera e exibe a matriz resultante da multiplicação
	// Matriz* matrizResultanteDevice = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);



	// Declara os ponteiro a serem passados para o device
	float* ptrMatrix;
	float* ptrMatrix2;
	float* ptrMatrixResult;
	int cudaStatus;



	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus = cudaMalloc(&ptrMatrix, matriz1->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaStatus = cudaMalloc(&ptrMatrix2, matriz2->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaStatus = cudaMalloc(&ptrMatrixResult, matrizResultadoGPU->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	dim3 numeroBlocos2D(matrizResultado->numeroLinhas,
		matrizResultado->numeroColunas);

	multiplicaoMatrixVetor(matriz1, matriz2, matrizResultado);

	// para cada teste
	for (int t = 0; t < test->qtdTestes; t++) {


		startTime();
		// copia o vetor de multiplicacao
		cudaMemcpy(ptrMatrix2, matriz2->matriz,
			matriz2->numeroLinhas * sizeof(float), cudaMemcpyHostToDevice);

		cudaDeviceSynchronize();

		// para cada stream
		for (int a = 0; a < size; a++) {
			// executar copia de memoria Async
			// copia so uma linha da matrix

			// deslocamento das matrizes
			long deslMatriz1 = (a * matriz1->numeroColunas);

			long deslMatrizResultado = a;

			cudaMemcpyAsync(ptrMatrix + deslMatriz1, matriz1->matriz + deslMatriz1,
				matriz1->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);

			/*
			matrixMultiStream << <1, 1, 0, stream[a] >> > (matriz1->numeroColunas, ptrMatrix + deslMatriz1,
				ptrMatrix2, ptrMatrixResult + deslMatrizResultado);

				*/

				// precisa copiar apenas um valor do vetor
			cudaMemcpyAsync(matrizResultadoGPU->matriz + deslMatrizResultado, ptrMatrixResult + deslMatrizResultado,
				sizeof(float), cudaMemcpyDeviceToHost, stream[a]);
			// executar o Kernel
			// cuda de memoria de volta para o host Async

		}
		// sincronizar os kernels depois das chamadas
		// contar o tempo de execucao
		cudaDeviceSynchronize();
		stopTime(true);
		for (int a = 0; a < size; a++) {
			int streamStatus = cudaStreamQuery(stream[a]);
			if (streamStatus != 0) {
				std::cout << "stream " << a << " não terminada.";
			}
		}
		/*
		std::cout << "Vetor Gpu resultado:";
		quebraLinha();
		printMatriz(matrizResultadoGPU);


		std::cout << "VETOR CPU: ";
		quebraLinha();



		quebraLinha();


		std::cout << "Resultado:" << std::endl;
		printMatriz(matrizResultado);
		quebraLinha();

		*/
		quebraLinha();

		if (isEqualMatrix(matrizResultadoGPU, matrizResultado)) {
			std::cout << "Teste concluido " << t << " com sucesso";
		}
		else {
			std::cout << "Ocorreu um erro no teste " << t;
		}


		/*
		std::cout << "REsultado do tempo ";
		quebraLinha();
		printTimeResult();
		*/
	}

	printTimeResult();

	for (int i = 0; i < size; ++i) {
		cudaStreamDestroy(stream[i]);
	}

	cudaFree(ptrMatrix);
	cudaFree(ptrMatrix2);
	cudaFree(ptrMatrixResult);

	freeMatrizStream(matriz1);
	freeMatrizStream(matriz2);
	freeMatrizStream(matrizResultado);

	// criar os ponteiros para a GPU

	// para cada teste
		// realizar a conta CPU
		// realizar a conta GPU
		// sincronizar

	// verificar se a conta esta correta 
	// mostrar resultados

	// desalocar os ponteiros


}

void testeMultiplicao() {
	clearResult();
	// criar as matrizes do host
	std::cout << "Teste de Multicao sem stream e usando 1 bloco de threads do tamanho da matriz resultante" << std::endl;


	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);

	printf("Device Number: %d\n", 0);
	printf("  Device name: %s\n", prop.name);
	printf("  Memory Clock Rate (KHz): %d\n",
		prop.memoryClockRate);
	printf("  Memory Bus Width (bits): %d\n",
		prop.memoryBusWidth);

	printf(" Device asyncEngineCount : %d\n",
		prop.asyncEngineCount);

	// define o teste
	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = QTD_TESTS;
	test->sizeMatrix = SIZE_MATRIX;

	int size = test->sizeMatrix;

	// cria as streams
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}


	// criar as struct das matrizes
	Matriz* matriz1 = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matriz1);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* matriz2 = gerarStructMatriz(test->sizeMatrix, 1);
	/*
	alocarMatrizCudaStream(matriz2);
	std::cout << "matrix 1 : ";
	quebraLinha();
	printMatriz(matriz1);
	quebraLinha();
	std::cout << "matrix 2 : ";
	quebraLinha();
	printMatriz(matriz2);
	*/


	if (podeMultiplicar(matriz1, matriz2)) {
		std::cout << "pode multiplicar";
		quebraLinha();
	}
	else {
		std::cout << "Nao pode multiplicar";
		quebraLinha();
		return;
	}

	Matriz* matrizResultado = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultado);

	Matriz* matrizResultadoGPU = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultadoGPU);

	//std::cout << "matrix Resulktado inciail : ";
	//quebraLinha();
	//printMatriz(matrizResultado);


	// Gera e exibe a matriz resultante da multiplicação
	// Matriz* matrizResultanteDevice = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);



	// Declara os ponteiro a serem passados para o device
	float* ptrMatrix;
	float* ptrMatrix2;
	float* ptrMatrixResult;
	int cudaStatus;



	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus = cudaMalloc(&ptrMatrix, matriz1->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaStatus = cudaMalloc(&ptrMatrix2, matriz2->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaStatus = cudaMalloc(&ptrMatrixResult, matrizResultadoGPU->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	dim3 numeroBlocos2D(matrizResultado->numeroLinhas,
		matrizResultado->numeroColunas);

	multiplicaoMatrixVetor(matriz1, matriz2, matrizResultado);

	// para cada teste
	for (int t = 0; t < test->qtdTestes; t++) {


		startTime();

		// copiar os dados para a gpu

		cudaStatus = cudaMemcpy(ptrMatrix, matriz1->matriz,
			matriz1->tamanhoFloatArray, cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
		}

		// Copia o vetor de multiplicação para a memória alocada no device
		cudaStatus = cudaMemcpy(ptrMatrix2, matriz2->matriz,
			matriz2->tamanhoFloatArray, cudaMemcpyHostToDevice);

		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
		}

		// executar o kernel

		/*

		matrixMulti << <1, numeroBlocos2D >> > (
			matriz1->numeroLinhas,
			matriz1->numeroColunas, ptrMatrix,
			matriz2->numeroLinhas,
			matriz2->numeroColunas, ptrMatrix2, ptrMatrixResult);
			*/

			// copiar a volta
		cudaMemcpy(matrizResultadoGPU->matriz, ptrMatrixResult,
			matrizResultadoGPU->tamanhoFloatArray, cudaMemcpyDeviceToHost);

		cudaDeviceSynchronize();
		stopTime(true);
		/*
		std::cout << "Vetor Gpu resultado:";
		quebraLinha();
		printMatriz(matrizResultadoGPU);


		std::cout << "VETOR CPU: ";
		quebraLinha();

		multiplicaoMatrixVetor(matriz1, matriz2, matrizResultado);

		quebraLinha();


		std::cout << "Resultado:" << std::endl;
		printMatriz(matrizResultado);
		quebraLinha();
		*/


		if (isEqualMatrix(matrizResultadoGPU, matrizResultado)) {
			std::cout << "Teste concluido " << t << " com sucesso";
		}
		else {
			std::cout << "Ocorreu um erro no teste " << t;
		}
	}
	quebraLinha();
	printTimeResult();




	for (int i = 0; i < size; ++i) {
		cudaStreamDestroy(stream[i]);
	}

	cudaFree(ptrMatrix);
	cudaFree(ptrMatrix2);
	cudaFree(ptrMatrixResult);

	freeMatrizStream(matriz1);
	freeMatrizStream(matriz2);
	freeMatrizStream(matrizResultado);

	// criar os ponteiros para a GPU

	// para cada teste
		// realizar a conta CPU
		// realizar a conta GPU
		// sincronizar

	// verificar se a conta esta correta 
	// mostrar resultados

	// desalocar os ponteiros


}

void testeMultiplicaoStreamKernel() {
	clearResult();

	// criar as matrizes do host
	std::cout << "Teste de Multi Stream inicial, sem usar threads" << std::endl;


	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);

	printf("Device Number: %d\n", 0);
	printf("  Device name: %s\n", prop.name);
	printf("  Memory Clock Rate (KHz): %d\n",
		prop.memoryClockRate);
	printf("  Memory Bus Width (bits): %d\n",
		prop.memoryBusWidth);

	printf(" Device asyncEngineCount : %d\n",
		prop.asyncEngineCount);

	// define o teste
	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = QTD_TESTS;
	test->sizeMatrix = SIZE_MATRIX;

	int size = test->sizeMatrix;

	// cria as streams
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}


	// criar as struct das matrizes
	Matriz* matriz1 = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);
	alocarMatrizCudaStream(matriz1);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* matriz2 = gerarStructMatriz(test->sizeMatrix, 1);
	alocarMatrizCudaStream(matriz2);

	/*
	std::cout << "matrix 1 : ";
	quebraLinha();
	printMatriz(matriz1);
	quebraLinha();
	std::cout << "matrix 2 : ";
	quebraLinha();
	printMatriz(matriz2);
	*/


	if (podeMultiplicar(matriz1, matriz2)) {
		std::cout << "pode multiplicar";
		quebraLinha();
	}
	else {
		std::cout << "Nao pode multiplicar";
		quebraLinha();
		return;
	}

	Matriz* matrizResultado = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultado);

	Matriz* matrizResultadoGPU = gerarStructMatriz(matriz1->numeroLinhas, matriz2->numeroColunas);
	alocarMatrizCudaStream(matrizResultadoGPU);

	//std::cout << "matrix Resulktado inciail : ";
	//quebraLinha();
	//printMatriz(matrizResultado);


	// Gera e exibe a matriz resultante da multiplicação
	// Matriz* matrizResultanteDevice = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);



	// Declara os ponteiro a serem passados para o device
	float* ptrMatrix;
	float* ptrMatrix2;
	float* ptrMatrixResult;
	int cudaStatus;



	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus = cudaMalloc(&ptrMatrix, matriz1->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaStatus = cudaMalloc(&ptrMatrix2, matriz2->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaStatus = cudaMalloc(&ptrMatrixResult, matrizResultadoGPU->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	dim3 numeroBlocos2D(matriz1->numeroLinhas,
		matriz1->numeroColunas);

	multiplicaoMatrixVetor(matriz1, matriz2, matrizResultado);

	// para cada teste
	for (int t = 0; t < test->qtdTestes; t++) {


		startTime();
		// copia o vetor de multiplicacao
		cudaMemcpy(ptrMatrix2, matriz2->matriz,
			matriz2->numeroLinhas * sizeof(float), cudaMemcpyHostToDevice);

		cudaDeviceSynchronize();

		// para cada stream
		for (int a = 0; a < size; a++) {
			// executar copia de memoria Async
			// copia so uma linha da matrix

			// deslocamento das matrizes
			long deslMatriz1 = (a * matriz1->numeroColunas);

			long deslMatrizResultado = a;

			cudaMemcpyAsync(ptrMatrix + deslMatriz1, matriz1->matriz + deslMatriz1,
				matriz1->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);
			/*

			matrixMultiStreamKernel << <1, numeroBlocos2D, stream[a] >> > (matriz1->numeroColunas, ptrMatrix + deslMatriz1,
				ptrMatrix2, ptrMatrixResult + deslMatrizResultado);
				*/

				// precisa copiar apenas um valor do vetor
			cudaMemcpyAsync(matrizResultadoGPU->matriz + deslMatrizResultado, ptrMatrixResult + deslMatrizResultado,
				sizeof(float), cudaMemcpyDeviceToHost, stream[a]);
			// executar o Kernel
			// cuda de memoria de volta para o host Async

		}
		// sincronizar os kernels depois das chamadas
		// contar o tempo de execucao
		cudaDeviceSynchronize();
		stopTime(true);
		for (int a = 0; a < size; a++) {
			int streamStatus = cudaStreamQuery(stream[a]);
			if (streamStatus != 0) {
				std::cout << "stream " << a << " não terminada.";
			}
		}
		/*
		std::cout << "Vetor Gpu resultado:";
		quebraLinha();
		printMatriz(matrizResultadoGPU);


		std::cout << "VETOR CPU: ";
		quebraLinha();



		quebraLinha();


		std::cout << "Resultado:" << std::endl;
		printMatriz(matrizResultado);
		quebraLinha();

		*/
		quebraLinha();

		if (isEqualMatrix(matrizResultadoGPU, matrizResultado)) {
			std::cout << "Teste concluido " << t << " com sucesso";
		}
		else {
			std::cout << "Ocorreu um erro no teste " << t;
		}


		/*
		std::cout << "REsultado do tempo ";
		quebraLinha();
		printTimeResult();
		*/
	}

	printTimeResult();

	for (int i = 0; i < size; ++i) {
		cudaStreamDestroy(stream[i]);
	}

	cudaFree(ptrMatrix);
	cudaFree(ptrMatrix2);
	cudaFree(ptrMatrixResult);

	freeMatrizStream(matriz1);
	freeMatrizStream(matriz2);
	freeMatrizStream(matrizResultado);

	// criar os ponteiros para a GPU

	// para cada teste
		// realizar a conta CPU
		// realizar a conta GPU
		// sincronizar

	// verificar se a conta esta correta 
	// mostrar resultados

	// desalocar os ponteiros


}


// enclapesular as funcoes de forma a ser necessario apenas passar os ponteiros
void multiplicacaoMatrixVetorStream(Matriz* matriz, Matriz* vetor, Matriz* matrixResultado, Matriz* matrizResultadoGPU,
	float* ptrMatrix, float* ptrMatrix2, float* ptrMatrixResult, int size);

void multiplicacaoMatrixVetorStream(Matriz* matriz, Matriz* vetor, Matriz* matrixResultado, Matriz* matrizResultadoGPU,
	float* ptrMatrix, float* ptrMatrix2, float* ptrMatrixResult, int size) {

	// cria as streams
	cudaStream_t stream[SIZE_MATRIX];
	for (int i = 0; i < size; ++i) {
		cudaStreamCreate(&stream[i]);
	}

	cudaMemcpy(ptrMatrix2, vetor->matriz,
		vetor->numeroLinhas * sizeof(float), cudaMemcpyHostToDevice);

	cudaDeviceSynchronize();

	// executar copia de memoria Async
	// copia so uma linha da matrix
	// deslocamento das matrizes

	dim3 numeroBlocos2D(matriz->numeroColunas);

	for (int a = 0; a < size; a++) {

		long deslMatriz1 = (a * matriz->numeroColunas);

		long deslMatrizResultado = a;

		cudaMemcpyAsync(ptrMatrix + deslMatriz1, matriz->matriz + deslMatriz1,
			matriz->numeroColunas * sizeof(float), cudaMemcpyHostToDevice, stream[a]);


		// matriz->numeroLinhas, matriz->numeroColunas
		// vetor->numeroColunasvetor->numeroLinhas
		tcc_MatrixVectorMultiplyThread << <1, numeroBlocos2D, 1024, stream[a] >> > (ptrMatrix + deslMatriz1, ptrMatrix2,
			ptrMatrixResult + deslMatrizResultado, matriz->numeroLinhas, matriz->numeroColunas);


		// precisa copiar apenas um valor do vetor
		cudaMemcpyAsync(matrizResultadoGPU->matriz + deslMatrizResultado, ptrMatrixResult + deslMatrizResultado,
			sizeof(float), cudaMemcpyDeviceToHost, stream[a]);



	}

	// executar o Kernel
	// cuda de memoria de volta para o host Async

	cudaDeviceSynchronize();

	for (int i = 0; i < size; ++i) {
		cudaStreamDestroy(stream[i]);
	}

}

/* teste do moscardi */
void zonaDeTestes();

__global__ void kernalDeMultiplicacao(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante);

#include "../matrix-utils/matrix-utils.h"

void zonaDeTestes() {
	float* acessoComum;

	// Gera e exibe a matriz a ser multiplicada
	Matriz* matrizDeMultiplicacao = gerarStructMatriz(2, 3);

	// Preenche a matrix com valores conhecidos
	acessaVetorComoMatrix(&acessoComum, 0, 0, matrizDeMultiplicacao);
	*acessoComum = 0;
	acessaVetorComoMatrix(&acessoComum, 0, 1, matrizDeMultiplicacao);
	*acessoComum = 3;
	acessaVetorComoMatrix(&acessoComum, 0, 2, matrizDeMultiplicacao);
	*acessoComum = 5;
	acessaVetorComoMatrix(&acessoComum, 1, 0, matrizDeMultiplicacao);
	*acessoComum = 5;
	acessaVetorComoMatrix(&acessoComum, 1, 1, matrizDeMultiplicacao);
	*acessoComum = 5;
	acessaVetorComoMatrix(&acessoComum, 1, 2, matrizDeMultiplicacao);
	*acessoComum = 2;

	std::cout << "Exibe a matriz a ser multiplicada";
	quebraLinha();
	printMatriz(matrizDeMultiplicacao);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* vetorDeMultiplicacao = gerarStructMatriz(3, 1);

	// Prenche vetor com valores conhecidos
	acessaVetorComoMatrix(&acessoComum, 0, 0, vetorDeMultiplicacao);
	*acessoComum = 3;
	acessaVetorComoMatrix(&acessoComum, 1, 0, vetorDeMultiplicacao);
	*acessoComum = 4;
	acessaVetorComoMatrix(&acessoComum, 2, 0, vetorDeMultiplicacao);
	*acessoComum = 3;

	std::cout << "Exibe o vetor a ser multiplicado";
	quebraLinha();
	printMatriz(vetorDeMultiplicacao);

	// Verifica se a multiplicação é possível
	if (!verificarMultMatrizVetor(matrizDeMultiplicacao,
		vetorDeMultiplicacao)) {
		std::cout
			<< "Impossível multiplicar matrizes com os tamanhos estipulados";
		quebraLinha();
		return;
	}

	// Gera e exibe a matriz resultante da multiplicação
	Matriz* matrizResultanteDevice = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);
	std::cout << "Exibe matriz resultante antes do processamento";
	quebraLinha();
	printMatriz(matrizResultanteDevice);

	Matriz* matrizResultanteHost = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);

	// Declara os ponteiro a serem passados para o device
	float* matriz, * vetor, * resultante;

	// Aloca as variáveis relacionadas a matriz a ser multiplicada no device
	cudaMalloc(&matriz, matrizDeMultiplicacao->tamanhoFloatArray);

	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaMalloc(&vetor, vetorDeMultiplicacao->tamanhoFloatArray);

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaMalloc(&resultante, matrizResultanteDevice->tamanhoFloatArray);

	// Copia a matriz de multiplicação para a memória alocada no device
	cudaMemcpy(matriz, matrizDeMultiplicacao->matriz,
		matrizDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);

	// Copia o vetor de multiplicação para a memória alocada no device
	cudaMemcpy(vetor, vetorDeMultiplicacao->matriz,
		vetorDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);

	/* Aqui a mágica acontece **/

	/* Realizando multiplicação no host **/

	simularMultiplicacaoNoHost(matrizDeMultiplicacao, vetorDeMultiplicacao);

	/* Realizando multiplicação no host **/

	/* Realizando multiplicação no device **/
	dim3 numeroBlocos2D(matrizResultanteHost->numeroLinhas,
		matrizResultanteHost->numeroColunas);

	kernalDeMultiplicacao << <matrizDeMultiplicacao->numeroColunas, numeroBlocos2D >> > (
		matrizDeMultiplicacao->numeroLinhas,
		matrizDeMultiplicacao->numeroColunas, matriz,
		vetorDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas, vetor, resultante);

	cudaDeviceSynchronize();

	// Copia a matriz resultante do device para o host
	cudaMemcpy(matrizResultanteDevice->matriz, resultante,
		matrizResultanteDevice->tamanhoFloatArray, cudaMemcpyDeviceToHost);

	std::cout << "Exibe martiz resultante host após o processamento [DEVICE]";
	quebraLinha();
	printMatriz(matrizResultanteDevice);

	/* Realizando multiplicação no device **/

	/* Aqui a mágica acontece **/

// Remove do device as alocações relacionadas a mratiz que foi multiplicada
	cudaFree(matriz);

	// Remove do device as alocações relacionadas ao vetor que foi multiplicado
	cudaFree(vetor);

	// Remove do device a alocação da matriz resultante
	cudaFree(resultante);

	// Libera a memória do host
	desalocaStructMatriz(matrizDeMultiplicacao);
	desalocaStructMatriz(vetorDeMultiplicacao);
	desalocaStructMatriz(matrizResultanteDevice);
}

__global__ void kernalDeMultiplicacao(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante) {

	unsigned int posicaoResultante = threadIdx.x * numeroColunasVetor
		+ threadIdx.y;
	unsigned int posicaoMatriz = threadIdx.x * numeroColunasMatriz + blockIdx.x;
	unsigned int posicaoVetor = blockIdx.x * numeroColunasVetor + threadIdx.y;

	float valorResultante = resultante[posicaoResultante], valorMatriz =
		matriz[posicaoMatriz], valorVetor = vetor[posicaoVetor];

	printf(
		"Posições sendo acessadas!\nResultante: [%d,%d] = %d\nMatriz: [%d,%d] = %d\nVetor: [%d,%d] = %d\nConta sendo realizada no device %f + %f * %f\n\n",
		threadIdx.x, threadIdx.y, posicaoResultante, threadIdx.x,
		blockIdx.x, posicaoMatriz, blockIdx.x, threadIdx.y, posicaoVetor,
		valorResultante, valorMatriz, valorVetor);

	resultante[posicaoResultante] = resultante[posicaoResultante]
		+ matriz[posicaoMatriz] * vetor[posicaoVetor];

}




/*
TesteTime

*/


/*
Usar kernel com tamanho igual o tamanho da matrix resultante
*/
__global__ void matrixMulti(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante);

void testeMultiplicao();

void testeTime();

void testeStream();

__global__ void matrixMulti(unsigned int numeroLinhasMatriz,
	unsigned int numeroColunasMatriz, float* matriz,
	unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
	float* vetor, float* resultante) {

	unsigned int posicaoResultante = toIndex(threadIdx.x, threadIdx.y, numeroColunasVetor);
	unsigned int posicaoMatrizStart = toIndex(0, threadIdx.x, numeroColunasMatriz);
	unsigned int posicaoVetorStart = toIndex(0, 0, numeroColunasVetor);

	float valorResultante = 0.0f;
	for (int a = 0; a < numeroColunasMatriz; a++) {
		valorResultante += matriz[posicaoMatrizStart + a] * vetor[posicaoVetorStart + a];
	}
	resultante[posicaoResultante] = valorResultante;

}

void testeTime() {
	auto tempo_create_matrix = std::chrono::high_resolution_clock::now();

	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = 100;
	test->sizeMatrix = 36000;

	Matriz* matrizDeMultiplicacao = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* vetorDeMultiplicacao = gerarStructMatriz(test->sizeMatrix, 1);

	Matriz* matrizResultanteHost = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);


	// Gera e exibe a matriz resultante da multiplicação
	Matriz* matrizResultanteDevice = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);

	auto tempo_create_matrix_end = std::chrono::high_resolution_clock::now();
	int time = std::chrono::duration_cast<std::chrono::milliseconds>(tempo_create_matrix_end - tempo_create_matrix).count();
	std::cout << "tempo create matrix " << time << " milliseconds" << std::endl;

	// copiar matriz para a GPU

	// Declara os ponteiro a serem passados para o device
	float* testeMalloc;
	float* matriz;
	float* vetor;
	float* resultante;
	int cudaStatus;
	std::cout << " tamanho da matriz " << matrizDeMultiplicacao->tamanhoFloatArray / 1000000 << " MBs" << std::endl;

	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus = cudaMalloc(&matriz, matrizDeMultiplicacao->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaMalloc(&vetor, vetorDeMultiplicacao->tamanhoFloatArray);

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaMalloc(&resultante, matrizResultanteDevice->tamanhoFloatArray);


	auto tempo_transfer_init = std::chrono::high_resolution_clock::now();
	// Copia a matriz de multiplicação para a memória alocada no device


	cudaStatus = cudaMemcpy(matriz, matrizDeMultiplicacao->matriz,
		matrizDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	// Copia o vetor de multiplicação para a memória alocada no device
	cudaStatus = cudaMemcpy(vetor, vetorDeMultiplicacao->matriz,
		vetorDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);

	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}


	dim3 numeroBlocos2D(matrizResultanteHost->numeroLinhas,
		matrizResultanteHost->numeroColunas);
	// rodar os teste e fazer a contagem
	auto tempo_transfer_end = std::chrono::high_resolution_clock::now();
	int time_create = std::chrono::duration_cast<std::chrono::milliseconds>(tempo_transfer_end - tempo_transfer_init).count();
	std::cout << "tempo transfer to GPU " << time_create << " milliseconds" << std::endl;
	std::vector<int> result;

	for (int a = 0; a < test->qtdTestes; a++) {

		// current date/time based on current system

		// convert now to string for

		auto start = std::chrono::high_resolution_clock::now();

		// operation to be timed ...


		/*
		matrixMulti<<<matrizDeMultiplicacao->numeroColunas, numeroBlocos2D >> > (
			matrizDeMultiplicacao->numeroLinhas,
			matrizDeMultiplicacao->numeroColunas, matriz,
			vetorDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas, vetor, resultante);

			*/
		cudaStatus = cudaDeviceSynchronize();

		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
		}

		std::cout << "Teste " << a << std::endl;
		auto finish = std::chrono::high_resolution_clock::now();
		int time = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
		result.push_back(time);

		std::cout << time << "microsegundos \n";
		quebraLinha();
	}

	for (int a : result) {
		std::cout << a << std::endl;
	}
	int Num2;
	// std::cin >> Num2;


	// salvar o tempo em um arquivo

}

/*
Teste Multiplciation function
*/

void multiplicarMatrixVetor() {

	std::cout << "CUDA Program" << std::endl;

	Matriz* matrizDeExibicao = gerarStructMatriz(10, 10);
	preencherMatrizComNumeracaoCreescente(matrizDeExibicao);
	printMatriz(matrizDeExibicao);

	// gerar matriz
	Matriz* matrizDeMultiplicacao = gerarStructMatriz(10, 10);
	preencherMatrizComNumeracaoCreescente(matrizDeMultiplicacao);
	// gerar vetor
	Matriz* vetorDeMultiplicacao = gerarStructMatriz(10, 1);
	preencherMatrizComNumeracaoCreescente(vetorDeMultiplicacao);
	printMatriz(vetorDeMultiplicacao);
	// verificar se eles a multiplicacao eh possivel
	if (!verificarMultMatrizVetor(matrizDeMultiplicacao,
		vetorDeMultiplicacao)) {
		std::cout << "Matriz e vetor com tamanhos errados";
		return;
	}

	//gerar espaco para a matriz resultante
	Matriz* matrizResultante = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);
	preencherMatrizComNumeracaoCreescente(matrizResultante);
	std::cout << "Matriz e vetor com tamanhos corretos";
	quebraLinha();

	simularMultiplicacaoNoHost(matrizDeMultiplicacao, vetorDeMultiplicacao);

	//
	//	std::cout << "Print matriz entrada" << std::endl;
	//	printMatrizFlat(matrizDeMultiplicacao->matriz, matrizDeMultiplicacao->linhas, matrizDeMultiplicacao->colunas);
	//	std::cout << "Print vetor entrada" << std::endl;
	//	printMatrizFlat(v->vetor, v->linhas, v->colunas);
	//
	//	// alocar memoria no cuda
	float* matrizCuda;
	float* vetorCuda;
	float* matrizResultadoCuda;
	//
	cudaMalloc(&matrizCuda, matrizDeMultiplicacao->tamanhoFloatArray);
	cudaMalloc(&vetorCuda, vetorDeMultiplicacao->tamanhoFloatArray);
	cudaMalloc(&matrizResultadoCuda, matrizResultante->tamanhoFloatArray);

	// copiar valores
	cudaMemcpy(matrizCuda, matrizDeMultiplicacao->matriz,
		matrizDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);
	cudaMemcpy(vetorCuda, vetorDeMultiplicacao->matriz,
		vetorDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);
	// configurar valores do kernel e exucutar
	int threadsPerBlock = 1;
	dim3 blocksPerGrid(matrizResultante->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);
	multiplicacaoMatriz << <blocksPerGrid, threadsPerBlock >> > (matrizCuda,
		vetorCuda, matrizResultadoCuda, matrizDeMultiplicacao->numeroLinhas,
		matrizDeMultiplicacao->numeroColunas,
		vetorDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas, matrizResultante->numeroLinhas,
		matrizResultante->numeroColunas);

	cudaDeviceSynchronize();

	// trazer os valores de volta
	cudaMemcpy(matrizResultante->matriz, matrizResultadoCuda,
		matrizResultante->tamanhoFloatArray, cudaMemcpyDeviceToHost);
	std::cout << "matriz resultado" << std::endl;
	printMatriz(matrizResultante);
}




//__global__  void multiplicacaoMatriz(Matriz* matrizEntrada, Matriz* matrizVetor, Matriz* matrizResultado) {
//	// posicionar
//	// a matriz eh flat
//
////	float memCompartilhada = 0;
////	int posmatriz = (blockIdx.x * colunasMatriz) + threadIdx.x;
////	// a vetor eh flat e soh vou precisa de 1 elemento
////	int posvetor = threadIdx.x;
////	int posResultado = blockIdx.x;
////
////	int indexMaxMatriz = (linhasMatriz * colunasMatriz);
////	int indexMaxVetor = (linhasVetor * colunasVetor);
////	int indexMaxResultado = (linhasResultado * colunasResultado);
////
////	if (posmatriz < indexMaxMatriz && posvetor < indexMaxVetor) {
////		//float a = matrizEntrada[posmatriz];
////		//float b = vetorEntrada[posvetor];
////		// usar memoria do block
////		memCompartilhada += 3;
////
////	}
////	if (posResultado < indexMaxResultado) {
////		matrizResultado[posResultado] += memCompartilhada;
////	}
//
//}
