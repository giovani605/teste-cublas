#pragma once

#include <cuda.h>
#include <cuda_runtime.h>

typedef struct SpecTest
{
	int qtdTestes;
	int sizeMatrix;
};
#define SIZE_MATRIX 3
#define QTD_TESTS 10


struct  Matriz {
	float* matriz;
	int numeroLinhas;
	int numeroColunas;
	size_t tamanhoFloatArray;
} ;


__host__ __device__  int toIndex(int x, int y, int colunas);