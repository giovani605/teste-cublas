#include "common-utils.h"

__host__ __device__  int toIndex(int x, int y, int colunas) {
	return (y * colunas) + x;
}